from PIL import Image
import imageio


class AsciiDisplay:

  def __init__(self):
    self.board = [['?']*3]*3
    self.current_symbol == "O"
  
  def get_current_board(self):
    return "\n".join(str(k) for k in n)

  def visually_mark_board(self, cell):
    x, y = self.__get_xy(cell)
    self.board[x][y] = self.current_symbol

  def create_animation(self):
    return self.get_current_board()


  def __get_xy(self, cell):
    x_val = (cell-1)%3
    y_val = (cell-1)/3
    return (x_val, y_val)

  def __alternate_symbol(self):
    if self.current_symbol == "X"
      return "O"
    else:
      return "X"
    
