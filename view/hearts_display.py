from PIL import Image
import imageio


class HeartsDisplay:

	def __init__(self):
		empty_board_file = "./static/numbered_pillow.gif"		
		pink_heart_file = "./static/pink_heart.gif"
		blue_heart_file = "./static/blue_heart.gif"

		self.current_board_img = Image.open(empty_board_file).convert("RGBA")
		self.pink_heart_img = Image.open(pink_heart_file).convert("RGBA")
		self.blue_heart_img = Image.open(blue_heart_file).convert("RGBA")
		self.current_heart = self.blue_heart_img

		self.__xshift = [4, 35, 66]
		self.__yshift = [3, 34, 65]
		self.all_moves = [empty_board_file[:-4]]
		self.current_file_name = "./static/"
	
	def get_current_board(self):
		if self.current_file_name == "./static/":
			return "numbered_pillow"

		return self.current_file_name[9:]

	def visually_mark_board(self, cell):
		self.current_heart, abb = self.__alternate_hearts()
		shift = self.__get_xy_shift(cell)
		new_board_img = self.current_board_img.copy()
		new_board_img.paste(self.current_heart, shift, self.current_heart)

		#form file name for new image
		new_file_name = "".join([self.current_file_name,str(cell),abb])
		new_board_img.save(new_file_name+".gif","GIF")
		print("LOG: adding new file ", new_file_name+".gif")

		#add new board image in visual sequence of moves
		self.all_moves.append(new_file_name)
		print("LOG: updated all_moves ", self.all_moves)

		self.current_board_img = new_board_img
		self.current_file_name = new_file_name
		return self.current_board_img


	def create_animation(self):
		images = []
		filenames = self.all_moves
		print("LOG: creating animation with ", filenames)
		for filename in filenames:
			images.append(imageio.imread(filename+".gif"))

		final_filename = filenames[-1] + "_final.gif"
		imageio.mimsave(final_filename, images)

		return final_filename[9:-4]


	def __get_xy_shift(self, cell):
		x_val = (cell-1)%3
		y_val = (cell-1)/3
		return (self.__xshift[x_val], self.__yshift[y_val])

	def __alternate_hearts(self):
		if self.current_heart == self.pink_heart_img:
			return (self.blue_heart_img, 'b')
		else:
			return (self.pink_heart_img, 'p')	
		
