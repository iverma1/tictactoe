import web
import sys
import json
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from model.game import TictacGame
from model.game_state import GameState
from model.game_state import GetErrorMessage
from model.game_state import ResponseMessage
import logging as log

urls = (
  '/ttt', 'SlackServer',
  '/image', 'ImageServer'
)
msg = GetErrorMessage()
img_url = "https://blooming-plains-78086.herokuapp.com/image?file="

#Service class for handling /ttt comands from slack
class SlackServer:
    all_games = {}

    def POST(self):
      """ 
      outputs 1) text message or 2) json payload with image url to slack webhook, eg:
      {"attachments": [{"text": "my_text", "image_url": "url", "title": "Hears TTT"}]}
      """
      
      #get player/channel/command data from request
      data = web.input()
      print("LOG: incoming req", data) 
      player_name = data.user_name
      channel_name = data.channel_name
      message = data.text.split()[0].strip()


      #if session doesn't exist start new one or return existing
      game = self.create_new_session(channel_name)
      res_message = None

      #switch between different commands
      if message == "show_board":
        print("LOG: show board: for " + channel_name)
        res_message = game.show_board()
        print(res_message.getResponseCode())


      elif message == "stop_game":                  
        print("LOG: stop game for:  " + channel_name)
        res_message = game.stop_game()
        del self.all_games[channel_name]

      
      elif message.isdigit():                       #trying to make a board move
        print("LOG: make move {} for {} in channel ".format(message, 
          player_name, channel_name))
        res_message = self.make_move(game, player_name, message)
        
      elif message[0] == '@':                       #trying to start new game
        guest_name = message[1:]                    #remove @
        print("LOG: start new game for {}, between {} and {} ".format(
          channel_name, player_name, guest_name))
        res_message = self.start_game(game, player_name, guest_name)
        
      if not res_message:
        print("LOG: invalid input: {}".format(message))
        res_message = ResponseMessage(GameState.INVALID_INPUT)

      if res_message.getResponseCode() == GameState.WON or \
       res_message.getResponseCode() == GameState.DEADLOCK:
        	del self.all_games[channel_name]  


      http_res = self.create_response_message(game, res_message)
      print("LOG: returning response for: {}".format(channel_name))
      return http_res

        
    def create_new_session(self, channel_name):
      if channel_name not in self.all_games.keys():
        new_game = TictacGame()
        self.all_games[channel_name] = new_game
        print("LOG: new game started: {} ".format(channel_name))
        return new_game
      else:
      	return self.all_games[channel_name]  


    def start_game(self, game, player_name, guest_name):  
      print("LOG: old game state: " + str(game.get_game_state()))
      if game.get_game_state() == GameState.STOPPED:
        res = game.start_game(player_name, guest_name)
        return res  

    def make_move(self, game, player_name, move_num):  
      if game.get_game_state() == GameState.RUNNING:
        print('LOG: move: ' + move_num)
        res = game.make_move(player_name, move_num) 
        return res


    def create_response_message(self, game, res_message):
      json_res_body={}

      if self.is_image_response(res_message.getResponseCode()):       
        user_message = "{}{}".format(res_message.getResponseCodeMessage(),
          game.get_current_player())

        image_file_url = "{}{}{}".format(img_url, 
          res_message.getResponseMessage(), ".gif")

        json_res_img = {'title': 'Hearts Tic Tac Toe', 'text':user_message,
         'image_url' : image_file_url}

        json_res_body['attachments'] = [json_res_img]
        
      else:
        json_res_body['text'] = res_message.getResponseCodeMessage()

      
      json_res_body['response_type'] ='in_channel' #let everyone in channel view game
      web.header('Content-Type', 'application/json')
      res = json.dumps(json_res_body)
      return res  
      

    def is_image_response(self, res_code):
      return True if res_code in [GameState.STARTED, GameState.RUNNING, 
      GameState.WON, GameState.DEADLOCK] else False    

      
#Service class for sending back Hearts tictactoe images      
class ImageServer:
    def GET(self):
      """serves back image for request of format
      http://{{url}}/image?file=tictactoe.gif"""

      data = web.input()
      file = data.file
      f = open('./static/'+file, 'r')
      return f.read()


if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()      
