import unittest
import sys
from os import path, remove
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from view.hearts_display import HeartsDisplay
from pathlib import Path

class HeartsDisplayTests(unittest.TestCase):
    """Tests for `TictacGame"""

    def test_display_flow(self):
        self.display =HeartsDisplay()
        self.display.visually_mark_board(1)

        self.assertEqual(Path("./static/1p.gif").exists(), True)
        self.display.visually_mark_board(2)
        self.assertEqual(Path("./static/1p2b.gif").exists(), True)
        self.display.visually_mark_board(3)
        self.assertEqual(Path("./static/1p2b3p.gif").exists(), True)
        self.display.visually_mark_board(4)
        self.assertEqual(Path("./static/1p2b3p4b.gif").exists(), True)
        self.display.visually_mark_board(5)
        self.display.visually_mark_board(6)
        self.display.visually_mark_board(7)
        self.display.visually_mark_board(8)
        self.display.visually_mark_board(9)
        self.display.create_animation()
        self.assertEqual(Path("./static/final.gif").exists(), True)

        #cleanup
        # remove("./static/1p.gif")
        # remove("./static/1p2b.gif")
        # remove("./static/1p2b3p.gif")
        # remove("./static/1p2b3p4b.gif")
        # remove("./static/final.gif")

if __name__ == '__main__':
    unittest.main()



