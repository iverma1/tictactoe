import unittest
import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from model.board import TictacBoard

class TictacGameTestCase(unittest.TestCase):
    """Tests for TictacBoard"""

    def setup(self):
      self.board =TictacBoard(None)

    def test_win_diag1(self):
      self.board =TictacBoard(None)
      res = self.board.make_move("cross", 1)
      self.assertEqual(res, "RUNNING")
      res = self.board.make_move("nought", 3)
      res = self.board.make_move("cross", 5)
      res = self.board.make_move("cross", 9)
      self.assertEqual(res, "WON")


    def test_diag2(self):
      self.board =TictacBoard(None)
      res = self.board.make_move("cross", 3)
      res = self.board.make_move("cross", 5)
      res = self.board.make_move("cross", 7)
      self.assertEqual(res, "WON")

    def test_row1(self):
      self.board =TictacBoard(None)
      res = self.board.make_move("cross", 1)
      res = self.board.make_move("cross", 2)
      res = self.board.make_move("cross", 3)
      self.assertEqual(res, "WON")

    def test_col3(self):
      self.board =TictacBoard(None)
      res = self.board.make_move("cross", 3)
      res = self.board.make_move("cross", 6)
      res = self.board.make_move("cross", 9)
      self.assertEqual(res, "WON")

    def test_deadlock(self):
      self.board =TictacBoard(None)
      res = self.board.make_move("cross", 2)
      res = self.board.make_move("cross", 4)
      res = self.board.make_move("cross", 5)
      res = self.board.make_move("cross", 9)
      res = self.board.make_move("nought", 1)
      res = self.board.make_move("nought", 3)
      res = self.board.make_move("nought", 6)
      res = self.board.make_move("nought", 8)
      #res = self.board.make_move("nought", 7)
      
      self.assertEqual(res, "DEADLOCK")

      

if __name__ == '__main__':
    unittest.main()

