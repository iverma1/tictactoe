import unittest
import sys
from os import path
sys.path.append( path.dirname( path.dirname( path.abspath(__file__) ) ) )
from model.game import TictacGame
from model.game_state import GameState

class TictacGameTestCase(unittest.TestCase):
    """Tests for `TictacGame"""

    def test_game_flow(self):
        self.game = TictacGame()
        res = self.game.start_game('cross', 'nought')
        self.assertEqual(res[0], GameState.STARTED)
        self.assertEqual(str(self.game.get_current_player()), 'nought')

        res = self.game.make_move('cross', 9)
        self.assertEqual(res[0], GameState.INVALID_USER_TURN)
        self.assertEqual(str(self.game.get_current_player()), 'nought')
        self.assertEqual(self.game.get_game_state(), GameState.RUNNING)
    

if __name__ == '__main__':
    unittest.main()
