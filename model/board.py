from model.game_state import GameState
import logging as log

log.basicConfig(level=log.DEBUG, filename="logfile", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
class TictacBoard:
            
  def __init__(self, display_helper):
    self._display_helper = display_helper  
    self._open_cells = [1, 2, 3, 4, 5, 6, 7 ,8 ,9]

    self._can_win = { 'row_1': 'OPEN', 'row_2': 'OPEN', 'row_3': 'OPEN', \
          'col_1': 'OPEN', 'col_2': 'OPEN', 'col_3': 'OPEN', \
          'diag_1': 'OPEN', 'diag_2': 'OPEN' }

    self.status = GameState.RUNNING     


  def make_move(self, player_name, cell_number):
    #check if cell was previously taken 
    cell_number = int(cell_number)
    print("LOG: cell number recieved as ", cell_number)
    print("LOG: open_cells ", self._open_cells)

    if not cell_number in self._open_cells:
      return GameState.INVALID_MOVE 
    
    self._open_cells.remove(cell_number)
    self._display_helper.visually_mark_board(cell_number)

    #update can win status for all impacted paths
    for path in self._get_impacted_paths(cell_number):

      if not path in self._can_win.keys():
          continue    

      elif  self._can_win[path] == 'OPEN':
          self._can_win[path] = player_name

      elif self._can_win[path] == player_name:
          self._can_win[path] = player_name+'_TWICE'

      elif self._can_win[path] == player_name+'_TWICE':
          self.status = GameState.WON
          print('LOG: game won with path,' + path + self._can_win[path])

      else:
          del self._can_win[path]
          if not self._can_win:  
          #if all win-able paths were removed, board is tied
            self.status = GameState.DEADLOCK
            print('LOG: game deadlocked')
            
    return self.status      


  def get_display(self, game_end=False):
    return self._display_helper.create_animation() \
      if game_end else self._display_helper.get_current_board()



  def _get_impacted_paths(self, cell_number):
    """
        cell numbers on a visual board are 1 - 9
        this function returns row_# col_# and diag_#
        for the corresponding cell number

    """
    res = []
    row = 1 + (cell_number-1)/3
    col = 1 + (cell_number-1)%3
    res.append('row_'+str(row))
    res.append('col_'+str(col))

    if cell_number in [1, 5, 9]:
      res.append('diag_1')

    if cell_number in [3, 5, 7]:
      res.append('diag_2')  

    return res    
