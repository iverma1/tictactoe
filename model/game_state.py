from enum import Enum


class GameState(Enum):
   #board states
   RUNNING = 0
   WON = 1
   DEADLOCK = 2

   #errors
   INVALID_USER_TURN = 3
   INVALID_MOVE = 4
   INVALID_INPUT = 5

   #game states
   STOPPED = 6
   STARTED = 7




class GetErrorMessage:
  def __init__(self):
    self.messages = {}
    self.messages[GameState.WON] = "Game Over, Congratulations "
    self.messages[GameState.DEADLOCK] = "No one wins this time! "
    self.messages[GameState.INVALID_MOVE] = "Box is already filled"
    self.messages[GameState.INVALID_USER_TURN] = "Wrong player is making move"
    self.messages[GameState.INVALID_INPUT] = "Input not recognized"
    self.messages[GameState.STOPPED] = "Game is not running"
    self.messages[GameState.STARTED] = "Let's play Hearts TicTac. Make a move "
    self.messages[GameState.RUNNING] = "Make next move "

  def msg(self, key):
      return self.messages.get(key, "")

    
class ResponseMessage:
  msg = GetErrorMessage()

  def __init__(self, state_code, message=""):
    self.state_code = state_code
    self.message = message
    
  def getResponseCodeMessage(self):
    return self.msg.msg(self.state_code)

  def getResponseMessage(self):
    return self.message   

  def getResponseCode(self):
    return self.state_code   
