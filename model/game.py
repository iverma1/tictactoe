from player import CrossPlayer, NoughtPlayer
from model.game_state import GameState
from model.board import TictacBoard
from view.hearts_display import HeartsDisplay
from model.game_state import ResponseMessage


class TictacGame:
	def __init__(self):
		self._board = TictacBoard(HeartsDisplay()) #we can substitue any display
		self._game_state = GameState.STOPPED


	def start_game(self, originalPlayer, guestPlayer):
		self._game_state = GameState.RUNNING
		self._player1 = CrossPlayer(originalPlayer)
		self._player2 = NoughtPlayer(guestPlayer)
		
		self._current_player = self._player2 #let the guest make the first move
		print("LOG: changed game state to: {}, players: {}, {}".format(str(self._game_state),
		 str(self._player1), str(self._player2)))
		return ResponseMessage(self._game_state, self._board.get_display())


	def stop_game(self):
		self._game_state = GameState.STOPPED
		return ResponseMessage(self._game_state, self._board.get_display())

	def show_board(self):
		return ResponseMessage(self._game_state, self._board.get_display())

	def make_move(self, playername, cell_number):
		#check if player making move is correct
		if playername != str(self._current_player):
			return ResponseMessage(GameState.INVALID_USER_TURN, None)

		# make move on board
		self._game_state = self._board.make_move(playername, cell_number)

		if self._game_state == GameState.WON:
			self._game_state = GameState.STOPPED 
			return ResponseMessage(GameState.WON, self._board.get_display(game_end=True))

		if self._game_state == GameState.DEADLOCK:
			self._game_state = GameState.STOPPED 
			return ResponseMessage(GameState.DEADLOCK, self._board.get_display())	
		
		else:
			self.__alternate_player()		#switch users
			return ResponseMessage(self._game_state, self._board.get_display())
			

	def get_game_state(self):
		return self._game_state


	def get_current_player(self):
		return self._current_player


	def __alternate_player(self):
		self._current_player = self._player2 \
		if self._current_player == self._player1 else self._player1


